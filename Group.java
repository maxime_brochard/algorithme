import javax.crypto.spec.PSource;
import java.util.ArrayList;
import java.util.List;

import static java.lang.System.lineSeparator;

public class Group {
    protected List<Personne> groupList;

    public Group() {
        this.groupList = new ArrayList<>();
    }

    public String getGroupsAndPersons(List<Group> listOfGroups){
        String groupContent = new String();
        for (int y = 0; y < listOfGroups.size(); ++y) {
            groupContent =  groupContent + listOfGroups.get(y).getListOfPersons();
        }
        return groupContent;
    }

    public String getListOfPersons() {
        String valeur = new String();
        for (Personne p : groupList) {
            valeur = valeur + p.getName() + lineSeparator();
        }
        return valeur;
    }

    public void addPeople(Personne p){
        groupList.add(p);
    }

    @Override
    public String toString(){
        return this.getListOfPersons();
    }

}


