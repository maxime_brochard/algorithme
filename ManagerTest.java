import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static java.lang.System.lineSeparator;

public class ManagerTest {
    @Test
    void shouldReturnNbGroups() {

        //Créer une liste de type Personne
        List<Personne> list1 = new ArrayList<>();

        //Crée + ajout des Personnes a la liste
        list1.add(new Personne("Maxime"));
        list1.add(new Personne("Cynthia"));
        list1.add(new Personne("Erwan"));
        list1.add(new Personne("Baptiste"));
        list1.add(new Personne("Marion"));
        list1.add(new Personne("Joannie"));
        list1.add(new Personne("Aurelien"));
        list1.add(new Personne("P.A"));
        list1.add(new Personne("Stephane"));
        list1.add(new Personne("Charles"));
        list1.add(new Personne("Richard"));
        list1.add(new Personne("Sarah"));

        Manager m = new Manager();

        List<Group> returnList = m.algorithme(list1, 3);

        Assertions.assertEquals(3 , returnList.size());

    }

    @Test
    void shouldReturnContentOfPetitGroups(){
        //Créer une liste de type Personne
        List<Personne> list1 = new ArrayList<>();

        //Crée + ajout des Personnes a la liste
        list1.add(new Personne("Maxime"));
        list1.add(new Personne("Cynthia"));
        list1.add(new Personne("Erwan"));
        list1.add(new Personne("Baptiste"));
        list1.add(new Personne("Marion"));
        list1.add(new Personne("Joannie"));
        list1.add(new Personne("Aurelien"));
        list1.add(new Personne("P.A"));
        list1.add(new Personne("Stephane"));
        list1.add(new Personne("Charles"));
        list1.add(new Personne("Richard"));
        list1.add(new Personne("Sarah"));

        Manager m = new Manager();

        List<Group> returnList = m.algorithme(list1, 4);

        Assertions.assertEquals("Maxime" + lineSeparator() + "Cynthia" + lineSeparator() + "Erwan" + lineSeparator() +
                "Baptiste" + lineSeparator() + "Marion" + lineSeparator() + "Joannie" + lineSeparator() +
                "Aurelien" + lineSeparator() + "P.A" + lineSeparator() + "Stephane" + lineSeparator() +
                "Charles" + lineSeparator() + "Richard" + lineSeparator() + "Sarah" + lineSeparator(), returnList.toString());
    }
}

