import javax.print.attribute.standard.Fidelity;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Manager {

    public Manager() {
    }

    public List<Group> algorithme(List<Personne> inputList, int nbGroups) {

        //Gérer ici les exceptions

        //déclarer une nouvelle liste qui comportera des groupes
        List<Group> listOfGroups = new ArrayList<>();
        List<Personne> emptyPetitGroup = new ArrayList<>();

        for (int z = 0; z < nbGroups; z++) {
            Group g = new Group();
            listOfGroups.add(g);
        }

        List<Personne> listeSecondaire = new ArrayList<>();
        listeSecondaire.addAll(inputList);
        Collections.shuffle(listeSecondaire);

        //mélanger la liste initale
        //Collections.shuffle(inputList);

        //trouver le nombre de personnes par groupe
        int nbPersonByGroup = inputList.size() / nbGroups;
        int residu = inputList.size()%nbGroups;

        //répartir les personnes dans les groupes
        int max = 0;
        for (int y = 0; y < nbGroups; ++y) {
            for (int x = 0; x < nbPersonByGroup; x++) {
                listOfGroups.get(y).addPeople(listeSecondaire.get(max));
                ++max;
                if ((nbPersonByGroup*(y+1)) == max)
                    break;
            }
        }
        int index = max;
        int start = listeSecondaire.size()-max-residu;
        if(residu!=0){
            for (int r = index; r<(index+residu);r++){
                listOfGroups.get(start).addPeople(listeSecondaire.get(max));
                ++max;
                ++start;
            }
        }

        //retourner la liste des groupes
        return listOfGroups;
    }
}


