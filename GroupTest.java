import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import java.util.List;
import static java.lang.System.lineSeparator;

public class GroupTest {
    @Test
    void shouldReturnGroupElements() {
        //Créer une liste
        List<Personne> list1 = new ArrayList<>();
        //Créer des personnes
        Personne p1 = new Personne("Maxime");
        Personne p2 = new Personne("Cynthia");
        Personne p3 = new Personne("Erwan");
        Personne p4 = new Personne("Michel");

        //Ajoute les personnes à la liste
        list1.add(p1);
        list1.add(p2);
        list1.add(p3);

        Group g1 = new Group();

        g1.addPeople(p4);


        Assertions.assertEquals("Maxime" + lineSeparator() + "Cynthia" + lineSeparator() +
                "Erwan" + lineSeparator() + "Michel" + lineSeparator(), g1.getListOfPersons());
    }

    @Test
    void shouldAddPeople(){
        //Créer une liste

        Personne p = new Personne("Maxime");
        Group group1 = new Group();
        group1.addPeople(p);

        Manager m = new Manager();
        Assertions.assertEquals("Maxime" + lineSeparator(), group1.getListOfPersons());
    }
}


