import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class PersonneTest {
    @Test
    void shouldReturnName() {
        Personne p = new Personne("Maxime");
        Assertions.assertEquals("Maxime", p.getName());
    }

}
