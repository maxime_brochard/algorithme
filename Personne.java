public class Personne {
    protected String name;

    public Personne(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
}
