import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        //Créer une liste de type Personne
        List<Personne> list1 = new ArrayList<>();

        //Crée + ajout des Personnes a la liste
        list1.add(new Personne("Maxime"));
        list1.add(new Personne("Cynthia"));
        list1.add(new Personne("Erwan"));
        list1.add(new Personne("Baptiste"));
        list1.add(new Personne("Marion"));
        list1.add(new Personne("Joannie"));
        list1.add(new Personne("Aurelien"));
        list1.add(new Personne("P.A"));
        list1.add(new Personne("Stephane"));
        list1.add(new Personne("Charles"));
        list1.add(new Personne("Richard"));
        list1.add(new Personne("Sarah"));

        //Nouvelle instance de Manager
        Manager m = new Manager();

        List<Group> dispatch = (m.algorithme(list1, 7));

        for (int y = 0; y < dispatch.size(); ++y) {
            System.out.println(dispatch.get(y));
        }
    }
}
